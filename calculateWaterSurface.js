function calculateWaterSurface(barrierHeights) {
    let totalSurface = 0;
    let leftMax = 0;
    let rightMax = 0;
    let left = 0;
    let right = barrierHeights.length - 1;

    while (left < right) {
        if (barrierHeights[left] < barrierHeights[right]) {
            if (barrierHeights[left] > leftMax) {
                leftMax = barrierHeights[left];
            } else {
                totalSurface += leftMax - barrierHeights[left];
            }
            left++;
        } else {
            if (barrierHeights[right] > rightMax) {
                rightMax = barrierHeights[right];
            } else {
                totalSurface += rightMax - barrierHeights[right];
            }
            right--;
        }
    }

    return totalSurface;
}

const barrierHeights = [2, 3, 5, 3, 1, 0, 2, 4, 7, 6, 1, 3, 0, 1, 3, 9, 6, 2, 1, 0, 1, 4, 7, 5, 1, 3, 2];
const waterSurface = calculateWaterSurface(barrierHeights);
console.log("Total water surface:", waterSurface);
